package com.qjc.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration // 启动时就要加载
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {

        return new Docket(DocumentationType.SWAGGER_2)
                .enable(true)
                .groupName("com.qjc.controller")
                .genericModelSubstitutes(DeferredResult.class)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.qjc.controller"))
                .paths(PathSelectors.any())
                .build().apiInfo(apiInfo());//.globalOperationParameters(pars);

    }

    private ApiInfo apiInfo() {

        return new ApiInfoBuilder().title("Demo server")
                .description("*****")
                .termsOfServiceUrl("https://www.cnblogs.com/java-spring")
                .version("1.0").build();
    }
}
