package com.qjc.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * @Description: 自定义过滤器1
 * @Author: qjc
 * @Date: 2020/4/20
 */
@Slf4j
/**
 * 如果用此注解，一定要在配置类中加另外一个注解：@ServletComponetScan，指定扫描的包。
 * 过滤器执行顺序会按照filterName的字母顺序进行
 * @WebFilter指定的过滤器优先级都高于FilterRegistrationBean配置的过滤器
 */
@WebFilter(urlPatterns = "/*", filterName = "aaaa")
public class TestFilter1 implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {
        log.info("过滤器1初始化！！！！");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.info("过滤器1开始！！！！");
        long start = System.currentTimeMillis();
        filterChain.doFilter(servletRequest, servletResponse);
        log.info("过滤器1结束！！！！耗时：" + (System.currentTimeMillis() - start) + "ms");
    }

    @Override
    public void destroy() {
        //当Filter被移除或服务器正常关闭时
        log.info("过滤器1销毁！！！！");
    }
}
