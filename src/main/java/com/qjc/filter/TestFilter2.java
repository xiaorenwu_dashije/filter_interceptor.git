package com.qjc.filter;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * @Description: 自定义过滤器2
 * @Author: qjc
 * @Date: 2020/4/20
 */
@Slf4j
@WebFilter(urlPatterns = "/*", filterName = "bbbb")
public class TestFilter2 implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {
        log.info("过滤器2初始化！！！！");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.info("过滤器2开始！！！！");
        long start = System.currentTimeMillis();
        filterChain.doFilter(servletRequest, servletResponse);
        log.info("过滤器2结束！！！！耗时：" + (System.currentTimeMillis() - start) + "ms");
    }

    @Override
    public void destroy() {
        //当Filter被移除或服务器正常关闭时
        log.info("过滤器2销毁！！！！");
    }
}
