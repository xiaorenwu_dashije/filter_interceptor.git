package com.qjc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan("com.qjc.filter")
public class FilterInteceptorApplication {

    public static void main(String[] args) {
        SpringApplication.run(FilterInteceptorApplication.class, args);
    }

}
