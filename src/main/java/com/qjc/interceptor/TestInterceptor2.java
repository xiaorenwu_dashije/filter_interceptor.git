package com.qjc.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description:
 * @Author: qjc
 * @Date: 2020/4/20
 */
@Slf4j
public class TestInterceptor2 implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("请求执行前拦截器22222开始！！！！");
        long start = System.currentTimeMillis();
        log.info("请求执行前拦截器22222结束！！！！耗时：" + (System.currentTimeMillis() - start) + "ms");
        return true;//当返回true的时候，才可以执行postHandle/afterCompletion
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.info("请求执行完成后，拦截器22222开始！！！！");
        long start = System.currentTimeMillis();
        log.info("请求执行完成后，拦截器22222结束！！！！耗时：" + (System.currentTimeMillis() - start) + "ms");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.info("视图渲染完成后，拦截器22222开始！！！！");
        long start = System.currentTimeMillis();
        log.info("视图渲染完成后，拦截器22222结束！！！！耗时：" + (System.currentTimeMillis() - start) + "ms");
    }
}
