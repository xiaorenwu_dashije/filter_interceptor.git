package com.qjc.controller;

import com.qjc.vo.ResponseBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @Description:
 * @Author: qjc
 * @Date: 2020/4/20
 */
@Api(value = "controller操作", tags = {"controller操作"})
@RequestMapping("/test")
@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
@Slf4j
public class TestController {

    @ApiOperation(value = "查询数据", notes = "查询数据")
    @RequestMapping(value = "/find/dataList", method = RequestMethod.POST)
    public ResponseBean findInterfaceList(@RequestParam String userName) {
        log.info("查询数据成功！！！！");
        return new ResponseBean(200, "查询成功", userName);
    }


}
