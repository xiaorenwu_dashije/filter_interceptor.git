package com.qjc.config;

import com.qjc.interceptor.TestInterceptor1;
import com.qjc.interceptor.TestInterceptor2;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Description: 拦截器配置
 * @Author: qjc
 * @Date: 2020/4/20
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration interceptorRegistration1 = registry.addInterceptor(new TestInterceptor1());
        interceptorRegistration1.addPathPatterns("/**");
        interceptorRegistration1.order(1);

        InterceptorRegistration interceptorRegistration2 = registry.addInterceptor(new TestInterceptor2());
        interceptorRegistration2.addPathPatterns("/**");
        interceptorRegistration2.order(2);
    }
}
