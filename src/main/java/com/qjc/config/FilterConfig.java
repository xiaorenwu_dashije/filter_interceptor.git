//package com.qjc.config;
//
//import com.qjc.filter.TestFilter1;
//import com.qjc.filter.TestFilter2;
//import org.springframework.boot.web.servlet.FilterRegistrationBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @Description: 如果不用@WebFilter注解，则需要此过滤器配置来实例化Filter类
// * @Author: qjc
// * @Date: 2020/4/20
// */
//@Configuration
//public class FilterConfig {
//
//    @Bean
//    public FilterRegistrationBean registFilter1() {
//        FilterRegistrationBean registration = new FilterRegistrationBean();
//        registration.setFilter(new TestFilter1());//设置过滤器
//        registration.addUrlPatterns("/*");//设置过滤URL
//        registration.setName("TestFilter1");//设置过滤器名称
//        registration.setOrder(1);//设置过滤器执行顺序
//        return registration;
//    }
//
//    @Bean
//    public FilterRegistrationBean registFilter2() {
//        FilterRegistrationBean registration = new FilterRegistrationBean();
//        registration.setFilter(new TestFilter2());
//        registration.addUrlPatterns("/*");
//        registration.setName("TestFilter2");
//        registration.setOrder(2);
//        return registration;
//    }
//
//
//}
